package com.gul.miyahome;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ora on 8/11/2017.
 */
@RestController
@RequestMapping("/")
public class HomeContrl {

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public String home(){
        return "I'm home";
    }
}
