package com.gul.miyahome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiyaHomeApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiyaHomeApplication.class, args);
	}
}
